#pragma once

#include <iostream>
#include <conio.h>

using namespace std;

class TicTacToe 
{
private:

	//Variables
	char m_board[9];
	int m_numTurns;
	char m_playerTurn;
	char m_winner;

public:
	
	//Constructor
	TicTacToe() {
		for (int i = 0; i < 9; i++)
		{
			m_board[i] = '-';
		}
		m_numTurns = 9;
		m_playerTurn = 'x';
		m_winner = ' ';
	}

	//Methods
	void DisplayBoard()
	{
		int count = 1;
		for (char space : m_board)
		{
			cout << space << " ";
			if (count == 3 || count == 6 || count == 9)
			{
				cout << "\n";
			}
			count++;
		}
			
	}

	bool IsOver()
	{
		if (m_board[0] == m_board[1] && m_board[1] == m_board[2] && m_board[1] != '-')
		{
			m_winner = GetWinnerName();
			return true;
		}

		else if (m_board[3] == m_board[4] && m_board[4] == m_board[5] && m_board[4] != '-')
		{
			m_winner = GetWinnerName();
			return true;
		}
		else if (m_board[6] == m_board[7] && m_board[7] == m_board[8] && m_board[7] != '-')
		{
			m_winner = GetWinnerName();
			return true;
		}
		else if (m_board[0] == m_board[3] && m_board[3] == m_board[6] && m_board[3] != '-')
		{
			m_winner = GetWinnerName();
			return true;
		}
		else if (m_board[1] == m_board[4] && m_board[4] == m_board[7] && m_board[4] != '-')
		{
			m_winner = GetWinnerName();
			return true;
		}
		else if (m_board[2] == m_board[5] && m_board[5] == m_board[8] && m_board[5] != '-')
		{
			m_winner = GetWinnerName();
			return true;
		}
		else if (m_board[0] == m_board[4] && m_board[4] == m_board[8] && m_board[4] != '-')
		{
			m_winner = GetWinnerName();
			return true;
		}
		else if (m_board[2] == m_board[4] && m_board[4] == m_board[6] && m_board[4] != '-')
		{
			m_winner = GetWinnerName();
			return true;
		}
		else if (m_board[0] != '-' && m_board[1] != '-' && m_board[2] != '-'
			&& m_board[3] != '-' && m_board[4] != '-' && m_board[5] != '-'
			&& m_board[6] != '-' && m_board[7] != '-' && m_board[8] != '-')
		{
			m_winner = 'T';
			return true;
		}
		else
			return false;
	}

	char GetPlayerTurn()
	{
		if (m_playerTurn == 'x')
		{
			return 'x';
		}
		else
		{
			return 'o';
		}
	}

	char GetWinnerName()
	{
		if (GetPlayerTurn() == 'x')
		{
			return 'o';
		}
		else
		{
			return 'x';
		}
	}

	bool IsValidMove(int position)
	{
		if (m_board[position-1] == '-')
			return true;
		else
			return false;
	}

	void Move(int position)
	{
		if (GetPlayerTurn() == 'x')
		{
			m_board[position-1] = 'x';
			m_playerTurn = 'o';
		}else if (GetPlayerTurn() == 'o')
		{
			m_board[position-1] = 'o';
			m_playerTurn = 'x';
		}

	}

	void DisplayResult()
	{
		if (m_winner == 'x')
		{
			cout << "The Winner is X!!!!";
		}
		else if (m_winner == 'o')
		{
			cout << "The Winner is O!!!!";
		}
		else if (m_winner == 'T')
		{
			cout << "The game is a Tie! :O ";
		}
	}
};